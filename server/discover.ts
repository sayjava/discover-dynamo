import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult,
  Handler
} from "aws-lambda";

import datasource from "./datasource";

const { stringify } = JSON;

export const discover: Handler<
  APIGatewayProxyEvent,
  APIGatewayProxyResult
> = async function hello(event, context, callback) {
  const source = datasource();

  try {
    const res = await source();

    return {
      body: stringify(res),
      statusCode: 200
    };
  } catch (error) {
    console.log(error);

    return {
        body: error.message,
        statusCode: 500
    }
  }
};
