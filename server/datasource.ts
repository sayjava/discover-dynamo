import * as AWS from "aws-sdk";
import { GetItemInput } from "aws-sdk/clients/dynamodb";

interface AWSConfig {
  endpoint?: string;
  region: string;
  accessKeyId: string;
  secretAccessKey: string;
}

export const LOCAL_CONFIG = <AWSConfig>{
  region: "us-west-2",
  endpoint: "http://localhost:8000",
  accessKeyId: "xxxx",
  secretAccessKey: "xxxx"
};

export default (config: AWSConfig = LOCAL_CONFIG): any => {
  AWS.config.update(config);

  const dynamoDb = new AWS.DynamoDB.DocumentClient();

  return (): Promise<any> => {
    const params = <GetItemInput>{
      TableName: "Album",
      Key: {
        AlbumId: 1,
        ArtistId: 1
      },
      ConsistentRead: false
    };
    return dynamoDb.get(params).promise();
  };
};
